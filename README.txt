fake-sapi module for Funambol
----------------------------

This module implements a portion of the Funambol Cloud API (known also as SAPI=Server API) 
that is required to get the new OneMediaHub clients to sync with a Funambol
Community Edition server, namely authentication and several dummy servlets.

It is possible we could leverage this module in other ways, i.e provide
the sync-on-updates functionality present in Funambol CarEd or
as a base for an open source media sync solution.

- Mathew McBride
<matt@bionicmessage.net>

