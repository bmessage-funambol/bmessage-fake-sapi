/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.bionicmessage.funambol.fakesapiwebapp;

import com.funambol.framework.core.Authentication;
import com.funambol.framework.core.Cred;
import com.funambol.framework.security.Officer;
import com.funambol.framework.server.Sync4jDevice;
import com.funambol.framework.server.Sync4jUser;
import com.funambol.server.admin.UserManager;
import com.funambol.server.config.Configuration;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author matt
 */
@WebServlet(name="loginServlet", urlPatterns={"/login"})
public class loginServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String actionParam = request.getParameter("action");
            if (!actionParam.equals("login"))
                return;
            String loginParam = request.getParameter("login");
            String passwordParam = request.getParameter("password");
            Configuration config = Configuration.getConfiguration();
            Sync4jDevice fakeSapiDevice = new Sync4jDevice("fake-sapi-device");
            config.getDeviceInventory().setDevice(fakeSapiDevice);

            System.out.println("Funambol Home: "+config.getConfigPath());
            Officer officer = config.getOfficer();
            Cred b64cred = new Cred(new Authentication(Cred.AUTH_TYPE_BASIC, loginParam, passwordParam));
            b64cred.getAuthentication().setDeviceId("fake-sapi-device");
            
            
            System.out.println(b64cred.getUsername());
            System.out.println(b64cred.getType());
            System.out.println(officer.toString());
            System.out.println(b64cred.getData());
            Sync4jUser isValidUser = officer.authenticateUser(b64cred);

            long now = System.currentTimeMillis();
            if (isValidUser != null) {
                HttpSession session = request.getSession(true);
                String jsonString = String.format("{\"responsetime\",\"%d\",\"data\": {\"jsessionid\":\"%s\"}}",now,session.getId());
                out.println(jsonString);
            } else {
                System.out.println("User "+loginParam+" not authenticated");
                response.setStatus(403); // Forbidden
            }
        } catch (Exception e) {
            e.printStackTrace(System.err);
        } finally {
            out.close();
        } 
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
