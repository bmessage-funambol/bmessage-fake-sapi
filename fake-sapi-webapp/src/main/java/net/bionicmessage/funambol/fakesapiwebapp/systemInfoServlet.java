/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package net.bionicmessage.funambol.fakesapiwebapp;

import com.funambol.framework.core.DevInf;
import com.funambol.framework.core.Ext;
import com.funambol.server.config.Configuration;
import com.funambol.server.config.ServerConfiguration;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author matt
 */
public class systemInfoServlet extends HttpServlet {

    private static final String template = "{\"portalurl\":\"https://onemediahub.com\","
                    + "\"hwv\":\"%s\",\"oem\":\"%s\",\"devid\":\"%s\",\"mod\":\"%s\","
                    + "\"utc\":\"%s\",\"mobileurl\":\"https://onemediahub.com/m\","
                    + "\"supportnumberofchanges\":\"%s\",\"verdtd\":\"%s\","
                    + "\"exts\":\"%s\","
                    + "\"devtyp\":\"%s\",\"supportlargeobjs\":\"%s\",\"sapiversion\":\"11.0.1\",\"fwv\":\"%s\",\"man\":\"%s\"}";
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            Configuration config = Configuration.getConfiguration();
            ServerConfiguration serverConfig = config.getServerConfig();
            DevInf devInf = serverConfig.getServerInfo();

            List<Ext> exts = devInf.getExts();
            StringBuilder extList = new StringBuilder();
            for(Ext ext: exts) {
                extList.append(ext.getXNam());
                extList.append(",");
            }

            if (extList.charAt(extList.length()-1) == ',')
                extList.deleteCharAt(extList.length()-1);
            

            String formatted = String.format(template, devInf.getHwV(), devInf.getOEM(),devInf.getDevID(),devInf.getMod(),devInf.getUTC().toString(),
                    devInf.getSupportNumberOfChanges().toString(),devInf.getVerDTD().getValue(),extList.toString(),devInf.getDevTyp(),
                    devInf.getSupportLargeObjs().toString(),devInf.getFwV(),devInf.getMan());
            out.println(formatted);
        } finally { 
            out.close();
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
